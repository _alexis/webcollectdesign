
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>InDesign - Dashboard</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME ICON STYLE  -->
    <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet" />
    <!-- PRETTYPHOTO STYLE  -->
    <link href="<?php echo base_url();?>assets/css/prettyPhoto.css" rel="stylesheet" />
    <!-- CUSTOM STYLE CSS -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" />
    <!-- CUSTOM THEME COLOR -- YOU CAN USE LIGHT & DARK THEMES  -->
    <link href="<?php echo base_url();?>assets/css/themes/dark.css" rel="stylesheet" />
    <!-- GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>

 <form action="<?php echo base_url('index.php/c_login/aksi_login'); ?>" method="post">
 <div class="form-group">
 <div class="row col-lg-4"></div>
        <div class="row col-lg-4">
        <br><br><br><br>
        <h1 align="center">Login</h1>


<!-- ini dipake bob supaya spannya ga keluar mulu -->
        <?php if($this->session->flashdata('pesan')!=NULL){?>
            <span class='alert alert-danger'><?php echo $this->session->flashdata('pesan');?> </span>
            <?php }else{ ?>
            <br><br>
            <?php };?>



            <label for="text">Username</label>
            <input type="text" class="form-control" placeholder="Username" name="username"><br>

            <label for="text">Password</label>
            <input type="password" class="form-control" placeholder="Password" name="pswrd"><br>
            
            <button type="submit" class="btn btn-default btn-primary">Login</button> 
            </div>
            <div class="row col-lg-4"></div>
            </div>
 </form>

   <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY  -->
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.1.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
    <!-- PRETTYPHOTO SCRIPTS -->
    <script src="<?php echo base_url();?>assets/js/jquery.prettyPhoto.js"></script>
    <!-- PORTFOLIO FILTER PLUGIN  -->
    <script src="<?php echo base_url();?>assets/js/jquery.mixitup.min.js"></script>
    <!-- NICE SCROLL SCRIPTS   -->
    <script src="<?php echo base_url();?>assets/js/jquery.nicescroll.min.js"></script>
    <!-- EASING SCROLL SCRIPTS   -->
    <script src="<?php echo base_url();?>assets/js/jquery.easing.min.js"></script>
    <!-- CUSTOM SCRIPTS  -->
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>

</body>
</html>
