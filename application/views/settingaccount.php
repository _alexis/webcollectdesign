
    
    <section id="features-sec">
        <div class="container">
            <div class="row text-left min-set">
                <div class="col-md-12">
                    <h2>Setting Account</h2>
                    <form action="#"></form>
                    <div class="form-group">
                            <div class="row col-xs-12">
                            <div class="row col-xs-4">
                                <label for="text">Name</label>
                                <input type="text" class="form-control" placeholder="Name" id="name"><br>
                                <label for="text">User Name</label>
                                <input type="text" class="form-control" placeholder="User Name" id="username"><br>
                                <label for="text">E-Mail</label>
                                <input type="text" class="form-control" placeholder="E-mail" id="email"><br>
                                </div>
                                <div class="row col-xs-4">        
                                <label for="text">Change Password</label>
                                <input type="password" class="form-control" placeholder="Password" id="pswrd" maxlength ="8"><br>
                                <label for="text">Repeat Password</label>
                                <input type="password" class="form-control" placeholder="Re-Password" id="repswrd" maxlength ="8"> <br>
                                </div>
                                
                            </div> 
                           
                            <div class="form-group">
                                <div class="row col-xs-12 text-right ">
                                    <button type="submit" class="btn btn-success btn-md " >Save</button> 
                                    <button type="submit" class="btn btn-basic " >Reset</button> 
                                </div>
                            </div>
                      </form> 
                </div>
            </div>    
        </div>
    </section>
    <!--END FEATURES SECTION-->
    
  