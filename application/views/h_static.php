﻿<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>InDesign</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet" />
   
    <!-- FONT AWESOME ICON STYLE  -->
    <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet" />
    <!-- PRETTYPHOTO STYLE  -->
    <link href="<?php echo base_url();?>assets/css/prettyPhoto.css" rel="stylesheet" />
    <!-- CUSTOM STYLE CSS -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" />
    <!-- CUSTOM THEME COLOR -- YOU CAN USE LIGHT & DARK THEMES  -->
    <link href="<?php echo base_url();?>assets/css/themes/dark.css" rel="stylesheet" />
    <!-- GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

     <link href="<?php echo base_url();?>assets/css/jquery.min.js"  />


</head>
<body>
    <div class="navbar navbar-inverse navbar-fixed-top scrollclass ">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="<?php echo base_url();?>assets/img/header.png"/></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">

               
                <?php  
                $sesstatus=$this->session->userdata('status');
                if($sesstatus=='AD'){ ?>    
                    <li><a href="<?php echo base_url();?>">Dashboard</a></li>
                    <li><a href="<?php echo base_url();?>index.php/c_adddetail">Add Design Detail</a></li>
                     <li><a href="<?php echo base_url();?>index.php/c_cekdetail">Check Design Detail</a></li>
                    <li><a href="<?php echo base_url();?>index.php/c_addaccount">Settings</a></li> &nbsp;
                    
                    <li><a href="<?php echo base_url();?>index.php/c_logout"><span class="glyphicon glyphicon-log-out" ></span></a></li>
                    
         <?php } else if($sesstatus=='DS') {?>
                    <li><a href="<?php echo base_url();?>">Dashboard</a></li>
                    <li><a href="<?php echo base_url();?>index.php/c_adddetail">Add Design Detail</a></li>
                    <li><a href="<?php echo base_url();?>index.php/c_cekdetail">Check Design Detail</a></li>
                    
                    
                    <li><a href="<?php echo base_url();?>index.php/c_login"><span class="glyphicon glyphicon-log-out" ></span></a></li>
                    <?php }else if ($sesstatus==NULL){ ?>     <?php }?> 
      
        
                

                </ul>
            </div>

        </div>
    </div>
    <!--END NAB BAR SECTION-->


  
