﻿
    
    <section id="features-sec">
        <div class="container">
            <div class="row text-left min-set">
                <div class="col-md-12">
                
                    <h2>Edit Design Detail</h2>
                    <?php foreach($dtl_design as $u){?>
                        <form action="<?php echo base_url().'index.php/c_cekdetail/update'; ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <div class="row col-md-3">
                                    <label for="text">ID</label>
                                    <input type="hidden" class="form-control" placeholder="ID"  name="nomor"value="<?php echo $u->nomor?>">
                                    <input type="text" class="form-control" placeholder="ID" disabled="disabled" value="<?php echo $u->nomor?>">
                                </div>
                                <br><br><br>                 
                                <div class="row col-md-6">
                                    <label for="text">Product Name</label>
                                    <input type="text" class="form-control" placeholder="Product Name" name="p_name" value="<?php echo $u->p_name?>">
                                </div> 
                                <div class="row col-md-12">
                                    <div class="row col-md-3">
                                        <label for="text">Approved by MD</label>
                                        <input type="date" class="form-control" name="app_md" value="<?php echo $u->app_md?>">
                                    </div>
                                    <div class="row col-md-3">
                                        <label for="text">Design Master Sent</label>
                                        <input type="date" class="form-control" name="d_master_sent" value="<?php echo $u->d_master_sent?>">
                                    </div>
                                    <div class="row col-md-3">
                                        <label for="text">Chromaline</label>
                                        <input type="date" class="form-control" name="chromaline" value="<?php echo $u->chromaline?>">
                                    </div>
                                    <div class="row col-md-3">
                                        <label for="text">Approved Packaging</label>
                                        <input type="date" format="non"class="form-control" name="app_packaging" value="<?php echo $u->app_packaging?>">
                                    </div>
                                </div>
                                <br><br>
                                <div class="row col-md-9">
                                    <label for="text">Printing by</label>
                                    <input type="text" class="form-control" id="printingby" name="printingby" value="<?php echo $u->printingby?>">
                                    <br>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="checked" >Checked  </label>
                                            <label><input type="checkbox" name="rek_md_ds">Merchandiser  </label>
                                            <label><input type="checkbox" name="rek_supp">Supplier  </label>
                                        </div>
                                        <div class="row col-md-3">
                                            <label for="sel1">Status </label>
                                            <select class="form-control" name="status" value="<?php echo $u->status?>">
                                                <option value="accept">ACCEPT</option>
                                                <option value="cancel">CANCEL</option>
                                            </select>
                                        </div>
                                        <div class="row col-md-3">
                                            <label for="text">Upload</label>
                                            
                                          
                                                <input type='file' name='userfile'   />
                                                <label for="sel1" > <?php echo $u->upload?></label>
                                        </div>
                                </div>
                                <br><br><br><br>
                                <div class="row col-md-12"><br>
                                    <button type="submit" class="btn btn-default">Submit</button>     
                                </div>
                            </div>
                            <br><br>                     
                        </form> 
                </div>
            </div>    
        </div>
                    <?php }?>
    </section>
    <!--END FEATURES SECTION-->
    
  