<br>

    
    <section id="features-sec">
        <div class="container">
            <div class="row text-left min-set">
                <div class="col-md-12">
                <?php if($this->session->flashdata('message')!=NULL){?>
                    <span class='alert alert-success'><?php echo $this->session->flashdata('message');?> </span>
                <?php }else{ }if($this->session->flashdata('pesan')!=NULL){ ?>
                    <span class='alert alert-danger alert-xs'><?php echo $this->session->flashdata('pesan');?> </span>
                    <?php }else{}?>
                    <h2>Manage Account</h2>
                </div>
                    <div class="row">
                <div class="col-md-8">
                    <h5 class="text-center"></h5>
                
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>No. Tlp</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>

                    <?php 
		                $no = 1;
		                foreach($pengguna as $u){ 
		            ?>

                </thead>
                <tbody>
                    <tr><td><?php echo $no++ ?></td>
                    <td><?php echo $u->Username ?></td>
                    <td><?php echo $u->Nama ?></td>
                    <td><?php echo $u->Email ?></td>
                    <td><?php echo $u->No_Telp ?></td>
                    <td><?php echo $u->Status_user ?></td>
                    <td><a href="<?php echo base_url().'index.php/c_addaccount/edit/'.$u->Username; ?>" class="btn btn-warning btn-xs" >Edit</a> <a href="<?php echo base_url().'index.php/c_addaccount/hapus/'.$u->Username; ?>" class="btn btn-danger btn-xs" >Delete</a>
                    
                    
                   
                </tbody>
                <?php } ?>
            </table>
            </div>


    <!--END FEATURES SECTION-->

<?php 
 //$sesstatus=$this->session->userdata('status');
//if($sesstatus =='AD') {?>

<form action="<?php echo base_url(). '/index.php/c_addaccount/tambah_aksi'; ?>" method="post">
    <div class="form-group">
        <br>
        <div class="row col-md-2">    <br>   

        <label for="text">Username</label>
            <input type="text" class="form-control" placeholder="Username" name="username"><br>

            <label for="text">Email</label>
            <input type="text" class="form-control" placeholder="Email" name="email"><br>

            <label for="text">Phone</label>
            <input type="text" class="form-control" placeholder="Phone" name="tlp"><br> 
            

        </div>
        <div class="row col-md-2">    <br>

        <label for="text">Name</label>
            <input type="text" class="form-control" placeholder="Name" name="Nama"><br>

            <label for="text">Password</label>
            <input type="password" class="form-control" placeholder="Password" name="pswrd" maxlength ="8"><br>
            <label for="sel1">Select Status </label>
            <select class="form-control" name="status">
                <option value="DS">Designer</option>
                <option value="MD">Merchandiser</option>
                <option value="AD">Admin</option>
            </select> <br>
            <button type="submit" class="btn btn-default btn-success">Add Account</button> 
            </div>
</div></div></div>
</div>
</form> 
<?php //}?>
</section>    


    <!--END JUST SECTION-->
    <section id="location-sec">
        <div class="container">
                    <div class="col-md-12 text-center">
                    <p>Develop by : _alexis</p>
                    <p>Contact : amansejahtera@hotmail.com</p>
                    <br />
                    <P>Design by : binarytheme.com</P>
                </div>
            </div>

        </div>
    </section>


  
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY  -->
    <script src="assets/js/jquery-1.11.1.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="assets/js/bootstrap.js"></script>
    <!-- PRETTYPHOTO SCRIPTS -->
    <script src="assets/js/jquery.prettyPhoto.js"></script>
    <!-- PORTFOLIO FILTER PLUGIN  -->
    <script src="assets/js/jquery.mixitup.min.js"></script>
    <!-- NICE SCROLL SCRIPTS   -->
    <script src="assets/js/jquery.nicescroll.min.js"></script>
    <!-- EASING SCROLL SCRIPTS   -->
    <script src="assets/js/jquery.easing.min.js"></script>
    <!-- CUSTOM SCRIPTS  -->
    <script src="assets/js/custom.js"></script>

</body>
</html>
