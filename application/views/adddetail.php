﻿
    
    <section id="features-sec">
        <div class="container">
            <div class="row text-left min-set">
                <div class="col-md-12">
                    <h2>Add Design Detail</h2>
                        <form action="<?php echo base_url().'index.php/c_adddetail/tambah_aksi'; ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <div class="row col-md-3">
                                    <label for="sel1">Select Homebrand </label>
                                        <select class="form-control" name="kode_homebrand">
                                            <option value="FOI">Indomaret - Food</option>
                                            <option value="NFI">Indomaret - Non Food</option>
                                            <option value="FOL">Larrist - Food</option>
                                            <option value="NFL">Larrist - Non Food</option>
                                            <option value="FON">NonHomeBrand - Food</option>
                                            <option value="NFN">NonHomeBrand  - Non Food</option>
                                        </select>
                                </div>
                                <div class="row col-md-2">
                                    <label for="text">Year</label>
                                    <input type="text" class="form-control" default="<?php echo date("Y");?>" placeholder="Year " name="tahun">
                                </div> 
                                <br><br><br>                 
                                <div class="row col-md-6">
                                    <label for="text">Product Name</label>
                                    <input type="text" class="form-control" placeholder="Product Name" name="p_name">
                                </div> 
                                <div class="row col-md-12">
                                    <div class="row col-md-3">
                                        <label for="text">Approved by MD</label>
                                        <input type="date" class="form-control" name="app_md">
                                    </div>
                                    <div class="row col-md-3">
                                        <label for="text">Design Master Sent</label>
                                        <input type="date" class="form-control" name="d_master_sent">
                                    </div>
                                    <div class="row col-md-3">
                                        <label for="text">Chromaline</label>
                                        <input type="date" class="form-control" name="chromaline">
                                    </div>
                                    <div class="row col-md-3">
                                        <label for="text">Approved Packaging</label>
                                        <input type="date" format="non"class="form-control" name="app_packaging">
                                    </div>
                                </div>
                                <br><br>
                                <div class="row col-md-9">
                                    <label for="text">Printing by</label>
                                    <input type="text" class="form-control" id="printingby" name="printingby">
                                    <br>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="checked" >Checked  </label>
                                            <label><input type="checkbox" name="rek_md_ds">Merchandiser  </label>
                                            <label><input type="checkbox" name="rek_supp">Supplier  </label>
                                        </div>
                                        <div class="row col-md-3">
                                            <label for="sel1">Status </label>
                                            <select class="form-control" name="status">
                                                <option value="accept">ACCEPT</option>
                                                <option value="cancel">CANCEL</option>
                                            </select>
                                        </div>
                                        <div class="row col-md-3">
                                            <label for="text">Upload</label>
                                            
                                          
                                                <input type='file' name='userfile'  />
                                                
                                        </div>
                                </div>
                                <br><br><br><br>
                                <div class="row col-md-12"><br>
                                    <button type="submit" class="btn btn-success">Submit</button>     
                                </div>
                            </div>
                            <br><br>                     
                        </form> 
                </div>
            </div>    
        </div>
    </section>
    <!--END FEATURES SECTION-->
    
  