<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_data extends CI_Model{
	
	function getLastInserted($table) {
		
		$Q = $this->db->query('SELECT max(id) as id from dtl_design' );
		$row = $Q->row_array();
		return $row['id'];
	 }

	function tampil_data($table){
		return $this->db->get($table)->result();
	}
 
	function input_data($data,$table){
		
		$this->db->insert($table,$data);
	
		
	}
 
	function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
		//$this->db->delete($table);
	}
 
	function edit_data($where,$table){		
		return $this->db->get_where($table,$where);
	}
 
	function update_data($where,$data,$table){
		
		//$this->db->where($where);
		$res = $this->db->update($table,$data,$where);
		return $res;
	}	

	function cek_login($table,$where){	
			
		$Q=$this->db->get_where($table,$where);
		$row=$Q->row_array();
		return $row['Status_user'];
	}	

	function cek_status($table,$where) {
		
		$Q = $this->db->get_where($table,$where);
		$row = $Q->row_array();
		return $row['Status_user'];
	 }
	 function data($table,$number,$offset){
		return $query = $this->db->get($table,$number,$offset)->result();		
	}
 
	function jumlah_data($table){
		return $this->db->get($table)->num_rows();
	}

	public function upload_file($filename){    
		$this->load->library('upload'); // Load librari upload        
		$config['upload_path'] = './excel/';    
		$config['allowed_types'] = 'xlsx';    
		$config['max_size']  = '2048';    
		$config['overwrite'] = true;    
		$config['file_name'] = $filename;      
		$this->upload->initialize($config); // Load konfigurasi uploadnya    
		if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil      
		// Jika berhasil :      
		$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');      
		return $return;    
		}else{      // Jika gagal :      
		$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());      
		return $return;    
		}  
	}  
		function get_cekdetail_list($limit, $start){
			$sql = 'select nomor, checked, d_master_sent, chromaline, app_packaging, printingby, rek_md_ds, rek_supp, upload, status from dtl_design order by id DESC limit ' . $start . ', ' . $limit   ;
			$query = $this->db->query($sql);
			return $query->result();

		} 


		
			 function caridata(){
			$c = $this->input->POST('cari');
			$this->db->like('p_name', $c);
			$query = $this->db->get ('dtl_design');
			return $query->result(); 
			 }
}