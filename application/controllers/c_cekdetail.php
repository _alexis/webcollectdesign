<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_cekdetail extends CI_Controller {

	function __construct()
	{
		parent::__construct();		
		$this->load->model('M_data');
		$this->load->helper (array('url'));
		//$this->load->library('unit_test');
		//$this->test();
		$this->load->database();
        $this->load->library('pagination');

	}

	// public function test(){
	// 	$test = 1 + 1;
	// 	$expected_result = 2;
	// 	$test_name ='Add one plus one';
	// 	echo $this->unit->run($test,$expected_result,$test_name);

	// }


	public function index()
	{
		$this->load->view('h_static');
		$this->load->database();
		$jumlah_data = $this->m_data->jumlah_data('dtl_design');

		//load the department_model
		//$this->m_data->tampil_data('dtl_design');
		
		$config['base_url'] = site_url('index.php/c_cekdetail/index/');
        $config['total_rows'] = $this->db->count_all('dtl_design');
        $config['per_page'] = "10";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] /  $config["per_page"];
        $config["num_links"] = floor($choice);

        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        //call the model function to get the department data
        $data['dtl_design'] = $this->m_data->get_cekdetail_list($config["per_page"], $data['page']);           

        $data['pagination'] = $this->pagination->create_links();

		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);		
		$data['dtl_design'] = $this->m_data->data('dtl_design',$config['per_page'],$from);
		$this->load->view('cekdetail',$data);

		
		//  echo"<pre>";
		//  print_r($from);
	 	//  echo "</pre>";

		
	}

	public function lakukan_download($file){
		
	
 		$data = file_get_contents('gambar/'.$file); // letak file pada aplikasi kita
 
 		force_download($file,$data);
		//force_download('localhost/webcollectdesign/gambar/'.$file,NULL);
	}	

	function hapus($nomor){
		$where = array('nomor' => $nomor);
		$this->m_data->hapus_data($where,'dtl_design');
		$this->session->set_flashdata('pesan', 'Detail Successfuly Delete');
		redirect('index.php/c_cekdetail/index');
	}
 
	function edit($nomor){
		$where = array('nomor' => $nomor);
		$data['dtl_design'] = $this->m_data->edit_data($where,'dtl_design')->result();
		$this->load->view('h_static');
		
		$this->load->view('editdetail',$data);
	}

	function update(){
		$nomor = $this->input->post('nomor');
		$p_name = $this->input->post('p_name');
		$app_md = $this->input->post('app_md');
		$checked = $this->input->post('checked');
		$d_master_sent = $this->input->post('d_master_sent');
		$chromaline = $this->input->post('chromaline');
		$app_packaging = $this->input->post('app_packaging');
		$printby = $this->input->post('printingby');
		$rek_md_ds = $this->input->post('rek_md_ds');
		$rek_supp = $this->input->post('rek_supp');
		$upload = $this->input->post('upload');
		$status = $this->input->post('status');
	 
		 $file = $this->input->post("userfille");
		 $config['upload_path']          = './gambar/';
		 $config['allowed_types']        = 'gif|jpg|png';
		 $config['max_size']             = 1000000000;
		
 
		 $this->load->library('upload', $config);
		 $upload = $_FILES["userfile"]["name"];  
 
		 
			
		$data = array(
			'p_name' => $p_name,
			'app_md' => $app_md,
			'checked' => $checked,
			'd_master_sent' => $d_master_sent,
			'chromaline' => $chromaline,
			'app_packaging' => $app_packaging,
			'printingby' => $printby,
			'rek_md_ds' => $rek_md_ds,
			'rek_supp' => $rek_supp,
			'upload' => $upload,
			'status' => $status
		);
	 
		$where = array( 'nomor' =>   $nomor);
		
// 	 echo"<pre>";
//   print_r($where);
// 	 echo "</pre>";
		$this->m_data->update_data($where,$data,'dtl_design');
		$sdata = array('upload_data' => $this->upload->data());
		$this->session->set_flashdata('message', 'Detail Successfuly Edited');
		redirect('index.php/c_cekdetail/index');
	}

		public function export(){

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		$excel = new PHPExcel();
		$excel->getProperties()->setCreator('My Notes Code')                 
		->setLastModifiedBy('WebCollectDesign')                 
		->setTitle("Detail Design")                 
		->setSubject("Designer")                 
		->setDescription("--")                 
		->setKeywords("--");

					$style_col = array(      
					'font' => array('bold' => true), // Set font nya jadi bold      
					'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)        
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)      
				),  'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis        
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis        
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis        
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis      
				));   
				
				// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel    
				$style_row = array(      'alignment' => array(        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)      
			),      'borders' => array(        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis        
			'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis        
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis        
			'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis     
			 )    );    
			 
			 $excel->setActiveSheetIndex(0)->setCellValue('A1', "DETAIL DESIGN");    
			 $excel->getActiveSheet()->mergeCells('A1:H1');   
			 $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);  
			 $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);    
			 $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1   
			  // Buat header tabel nya pada baris ke 3    
			  $excel->setActiveSheetIndex(0)->setCellValue('A3', "No");   
			  $excel->setActiveSheetIndex(0)->setCellValue('B3', "ID");     
			  $excel->setActiveSheetIndex(0)->setCellValue('C3', "Product Name");
			  $excel->setActiveSheetIndex(0)->setCellValue('D3', "Approved By MD");     
			  $excel->setActiveSheetIndex(0)->setCellValue('E3', "Design Master Sent"); 
			  $excel->setActiveSheetIndex(0)->setCellValue('F3', "Chromaline");       
			  $excel->setActiveSheetIndex(0)->setCellValue('G3', "Approved Packaging");    
			  $excel->setActiveSheetIndex(0)->setCellValue('H3', "Printing By");
			  $excel->setActiveSheetIndex(0)->setCellValue('I3', "Status");      
			




			  // Apply style header yang telah kita buat tadi ke masing-masing kolom header    
			  $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);    
			  $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);    
			  $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);    
			  $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);    
			  $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
			  $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);   
			  $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);       
			  $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);   
			  $excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);   
			  
			  
			 
			 
			 
			  // Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya    
			  $dtl_design = $this->m_data->tampil_data('dtl_design');    
			  $no = 1; // Untuk penomoran tabel, di awal set dengan 1    
			  $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4    
			  foreach($dtl_design as $data){ // Lakukan looping pada variabel siswa      
				$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);      
				$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->nomor);      
				$excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->p_name);     
				$excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->app_md);      
				$excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->d_master_sent);
				$excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->chromaline); 
				$excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->app_packaging);             
				$excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->printingby); 
				$excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $data->status); 
				 // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)      
				 $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);      
				 $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);      
				 $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);      
				 $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);      
				 $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
				 $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);            
				 $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row); 
				 $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row); 
				 $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row); 
				$no++; // Tambah 1 setiap kali looping     
				$numrow++; // Tambah 1 setiap kali looping    
				} 
				
				// Set width kolom    
				$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A    
				$excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); // Set width kolom B   
				 $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C    
				 $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D    
				 $excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);   
				 $excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);       
				 $excel->getActiveSheet()->getColumnDimension('G')->setWidth(30); 
				 $excel->getActiveSheet()->getColumnDimension('H')->setWidth(30); 
				 $excel->getActiveSheet()->getColumnDimension('I')->setWidth(30); 
				 // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)    
				 $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);    // Set orientasi kertas jadi LANDSCAPE    
				 $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);    // Set judul file excel nya    
				 $excel->getActiveSheet(0)->setTitle("Detail Design ");    $excel->setActiveSheetIndex(0);    
				 // Proses file excel    
				 header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');    
				 header('Content-Disposition: attachment; filename="DetailDesign.xlsx"'); // Set nama file excel nya    
				 header('Cache-Control: max-age=0');    
				 $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');    
				 $write->save('php://output');

	}

	//import langsung dari database
	function cari() {
		$data['dtl_design']=$this->m_data->caridata();
		//jika data yang dicari tidak ada maka akan keluar informasi 
		//bahwa data yang dicari tidak ada
		if($data['dtl_design']==null) {
		   print 'maaf data yang anda cari tidak ada atau keywordnya salah';
		   print br(2);
		   print anchor('cekdetail','kembali');
		   }
		   else {

			$data['pagination'] = $this->pagination->create_links();
			 $this->load->view('h_static');
		     $this->load->view('cekdetail',$data); 
 
 }
 }
	
 
}
