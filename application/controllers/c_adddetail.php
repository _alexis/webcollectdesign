<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_adddetail extends CI_Controller {

	function __construct()
	{
		parent::__construct();		
		$this->load->model('M_data');
		$this->load->helper(array('form','url'));
		//$this->load->library('upload', $config);
	}
	public function index()
	{
		$this->load->view('h_static');
		$this->load->view('adddetail',array('error' => ' ' ));
		;
		
		
	}

	
	function tambah_aksi(){
	
		$kode_homebrand = $this->input->post('kode_homebrand');
		$tahun = $this->input->post('tahun');
		$p_name = $this->input->post('p_name');
		$app_md = $this->input->post('app_md');
		$checked = $this->input->post('checked');
		$d_master_sent = $this->input->post('d_master_sent');
		$chromaline = $this->input->post('chromaline');
		$app_packaging = $this->input->post('app_packaging');
		$printby = $this->input->post('printingby');
		$rek_md_ds = $this->input->post('rek_md_ds');
		$rek_supp = $this->input->post('rek_supp');
		$status = $this->input->post('status');
		$lastid = $this->m_data->getLastInserted('dtl_design');
		$lastid + 1 ;
		$name = $this->session->userdata('Username');

		
		$file = $this->input->post("userfille");
		$config['upload_path']          = './gambar/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 1000000000;
			
	 
		$this->load->library('upload', $config);
		$upload = $_FILES["userfile"]["name"];  
	 
			 if ($file>0){
				$sdata = array('upload_data' => $this->upload->data());
				
				$data = array(
			
					'nomor' => $tahun."-".$kode_homebrand."".$lastid,
					'kode_homebrand' => $kode_homebrand,
					'tahun' => $tahun,
					'p_name' => $p_name,
					'app_md' => $app_md,
					'checked' => $checked,
					'd_master_sent' => $d_master_sent,
					'chromaline' => $chromaline,
					'app_packaging' => $app_packaging,
					'printingby' => $printby,
					'rek_md_ds' => $rek_md_ds,
					'rek_supp' => $rek_supp,
					'upload' => $upload,
					'status' => $status,
					'username' => $name
					);
				   $this->m_data->input_data($data,'dtl_design');
				   $pesan = "pesan" ;
				   $this->session->set_flashdata('message', 'Detail Successfuly Added');	 	
		  
				   redirect('index.php/c_cekdetail/index');


				
				
			}else{
				$data = array(
			
					'nomor' => $tahun."-".$kode_homebrand."".$lastid,
					'kode_homebrand' => $kode_homebrand,
					'tahun' => $tahun,
					'p_name' => $p_name,
					'app_md' => $app_md,
					'checked' => $checked,
					'd_master_sent' => $d_master_sent,
					'chromaline' => $chromaline,
					'app_packaging' => $app_packaging,
					'printingby' => $printby,
					'rek_md_ds' => $rek_md_ds,
					'rek_supp' => $rek_supp,
					'upload' => $upload,
					'status' => $status,
					'username' => $name
					);
				   $this->m_data->input_data($data,'dtl_design');
					 $pesan = "pesan" ;
		 		   $this->session->set_flashdata('message', 'Detail Successfuly Added');
				   redirect('index.php/c_cekdetail/index');
				
			
			}
		

	
 
		
		
		
	}
	
 
	
 
}
