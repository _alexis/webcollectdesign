<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_logout extends CI_Controller{

  function __construct(){
		parent::__construct();		
		$this->load->model('m_data');
 
	}
 
	function index(){
		$this->session->unset_userdata($data_session);
        $this->session->sess_destroy();
        redirect(base_url("index.php/c_login"));
	}}