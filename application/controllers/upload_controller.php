
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Upload_Controller extends CI_Controller {


public function __construct() {
parent::__construct();
$this->load->library('upload');

}


public function index(){
$this->load->view('custom_view.php', array('error' => ' ' ));
}


public function do_upload(){

    $name = $_FILES["userfile"]["name"];
    $ext = end((explode(".", $name))); # extra () to prevent notice    
$config = array(
'upload_path' => "./gambar/",
'allowed_types' => "gif|jpg|png|jpeg|pdf",
'overwrite' => TRUE,
'max_size' => "204800000000000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
'max_height' => "768000000",
'max_width' => "1020040000"
);


$this->load->library('upload', $config);
$this->upload->initialize($config);
if($this->upload->do_upload())
{
$data = array('upload_data' => $this->upload->data());
$this->load->view('upload_success',$data);
echo $name;
}
else
{
$error = array('error' => $this->upload->display_errors());
$this->load->view('custom_view', $error);
}
}
}
?>