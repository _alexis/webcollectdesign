<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_addaccount extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('m_data');
		$this->load->helper('url');
		//$this->load->library('unit_test');
		//$this->test();

	}
	//  public function test(){
	//  	$data['pengguna'] = $this->m_data->tampil_data('pengguna');
	// 	 $test = $data['pengguna'];
		 	
	// 	 $v = new stdClass();
	// 	 $v->Username = 'aman1';
	// 	 $v->Password = '827ccb0eea8a706c4c34a16891f84e7b';
	// 	 $v->Nama = 'amann';
	// 	 $v->Email = 'HAHA@gmail.com';
	// 	 $v->No_Telp = '8111';
	// 	 $v->Status_user = 'AD';
	// 	 $Array =  Array('0'=> $v);
        
			
		
		

	
	// ;
	// 	$expected_result = $Array;

	//  	$test_name ="test";
	//  	echo $this->unit->run($test,$expected_result,$test_name);

	//  }

	public function index()
	{
		
		$this->load->view('h_static'); //nampilin navbar dari view
		$data['pengguna'] = $this->m_data->tampil_data('pengguna');
		// print_r($data['pengguna']);
		// exit();
		$this->load->view('addaccount',$data); //nampilin halaman addaccount dr view
		
		
	}

    function tambah_aksi(){
    
        
    $username=$this->input->post('username');
	$password=$this->input->post('pswrd');
	$nama = $this->input->post('Nama');
	$email = $this->input->post('email');
	$tlp = $this->input->post('tlp');
    $status=$this->input->post('status');
   
 
		$data = array(
            'Username' =>$username,
			'Password'=>md5($password),
			'Nama'     =>$nama,
			'Email'    => $email,
			'No_Telp'  =>$tlp,
			'Status_user'=>$status
			
			);
		$this->m_data->input_data($data,'pengguna');
		$this->session->set_flashdata('message', 'Account successfuly added');
		redirect('index.php/c_addaccount');
	 
		
	}
	

	function update(){

		$username = $this->input->post('username');
		$password = $this->input->post('pswrd');
		$nama = $this->input->post('Nama');
		$email = $this->input->post('email');
		$tlp = $this->input->post('tlp');
    	// $status=$this->input->post('status');
		
		
		
		$status = $this->input->post('pekerjaan');
	 
		$data = array(
			
			'Password' =>md5($password),
			'Nama'     =>$nama,
			'Email'    => $email,
			'No_Telp'  =>$tlp,
            
		);
	 
		$where = array(
			'Username' => $username
		);
	 
		$this->m_data->update_data($where,$data,'pengguna');
		
		$this->session->set_flashdata('message', 'Account successfuly edited ');
		redirect('index.php/c_addaccount/index');
	}
 
	function hapus($username){
		$where = array('Username' => $username);
		$this->m_data->hapus_data($where,'pengguna');
		$this->session->set_flashdata('pesan', 'Account successfuly delete ');
		redirect('index.php/c_addaccount/index');
	}
 
	function edit($username){
		$where = array('Username' => $username);
		$data['pengguna'] = $this->m_data->edit_data($where,'pengguna')->result();
		$this->load->view('h_static');
		$this->load->view('editaccount',$data);
	}

	}