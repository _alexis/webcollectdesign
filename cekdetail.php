<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>InDesign - Add Detail Design</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME ICON STYLE  -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- PRETTYPHOTO STYLE  -->
    <link href="assets/css/prettyPhoto.css" rel="stylesheet" />
    <!-- CUSTOM STYLE CSS -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <!-- CUSTOM THEME COLOR -- YOU CAN USE LIGHT & DARK THEMES  -->
    <link href="assets/css/themes/dark.css" rel="stylesheet" />
    <!-- GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div class="navbar navbar-inverse navbar-fixed-top ">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="assets/img/header.png"/></a>
            </div>
            <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.php">Dashboard</a></li>
                    <li><a href="adddetail.php">Add Design Detail</a></li>
                    <li><a href="cekdetail.php">Check Design Detail</a></li>
                    <li><a href="settingaccount.php">Settings</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-log-out"></span></a></li>
                </ul>
            </div>

        </div>
    </div>
    <!--END NAB BAR SECTION-->
    <section id="features-sec">
        <div class="container">
<table>
    <thead></thead>
</table><table class="table table-striped table-hover table-sm">
                <thead>
                    <tr>
                        <th><small>Home Brand</small></th>
                        <th><small>Product Name</small></th>
                        <th><small>Approved by MD</small></th>
                        <th><small>Design Master Sent</small></th>
                        <th><small>Chromaline</small></th>
                        <th><small>Approved Packing</small></th>
                        <th><small>Printing by</small></th>
                        <th><small>Checked</small></th>
                        <th><small>Merchandiser</small></th>
                        <th><small>Supplier</small></th>
                        <th><small>Upload</small></th>
                        <th><small>Action</small></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td><button type="button" class="btn btn-primary btn-sm">EDIT</button>&nbsp;
            <button type="button" class="btn btn-danger btn-sm">DELETE</button></td>
                    </tr>
                    <tr>
                        <td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td><button type="button" class="btn btn-primary btn-sm">EDIT</button>&nbsp;
            <button type="button" class="btn btn-danger btn-sm">DELETE</button></td>
                    </tr>

                </tbody>
            </table>
</div>
</section>

    <!--END FEATURES SECTION-->
    
    <div id="just-sec" style="">
        <div class="overlay">
            + YOUR HEADLINE GOES HERE +
        </div>

    </div>
    <!--END JUST SECTION-->
    <section id="location-sec">
        <div class="container">
                    <div class="col-md-12 text-center">
                    <p>Develop by : _alexis</p>
                    <p>Contact : amansejahtera@hotmail.com</p>
                    <br />
                    <P>Design by : binarytheme.com</P>
                </div>
            </div>

        </div>
    </section>

  
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY  -->
    <script src="assets/js/jquery-1.11.1.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="assets/js/bootstrap.js"></script>
    <!-- PRETTYPHOTO SCRIPTS -->
    <script src="assets/js/jquery.prettyPhoto.js"></script>
    <!-- PORTFOLIO FILTER PLUGIN  -->
    <script src="assets/js/jquery.mixitup.min.js"></script>
    <!-- NICE SCROLL SCRIPTS   -->
    <script src="assets/js/jquery.nicescroll.min.js"></script>
    <!-- EASING SCROLL SCRIPTS   -->
    <script src="assets/js/jquery.easing.min.js"></script>
    <!-- CUSTOM SCRIPTS  -->
    <script src="assets/js/custom.js"></script>

</body>
</html>
