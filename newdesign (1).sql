-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2018 at 01:48 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newdesign`
--

-- --------------------------------------------------------

--
-- Table structure for table `dtl_design`
--

CREATE TABLE `dtl_design` (
  `id` int(3) NOT NULL,
  `nomor` varchar(11) NOT NULL,
  `kode_homebrand` char(3) NOT NULL,
  `tahun` int(4) NOT NULL,
  `p_name` varchar(100) DEFAULT NULL,
  `app_md` date DEFAULT NULL,
  `checked` tinyint(1) DEFAULT NULL,
  `d_master_sent` date DEFAULT NULL,
  `chromaline` date DEFAULT NULL,
  `app_packaging` date DEFAULT NULL,
  `printingby` varchar(50) DEFAULT NULL,
  `rek_md_ds` tinyint(1) DEFAULT NULL,
  `rek_supp` tinyint(1) DEFAULT NULL,
  `upload` varchar(50) DEFAULT NULL,
  `status` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dtl_design`
--

INSERT INTO `dtl_design` (`id`, `nomor`, `kode_homebrand`, `tahun`, `p_name`, `app_md`, `checked`, `d_master_sent`, `chromaline`, `app_packaging`, `printingby`, `rek_md_ds`, `rek_supp`, `upload`, `status`) VALUES
(1, '2018-FOI1', 'FOI', 2018, 'coba', '2018-04-11', 127, '2018-04-11', '2018-04-11', '2018-04-11', 'coba', 1, 1, 'casdfe23dsda3d3qd3', 'cancel'),
(3, '2018FOL', 'FOL', 2018, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '2018-FOL', 'FOL', 2018, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '2018-FOL', 'FOL', 2018, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '2018-FOL0', 'FOL', 2018, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '2018-FOL0', 'FOL', 2018, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '2018-NFI', 'NFI', 2018, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '2018-NFI2', 'NFI', 2018, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '2222', 'NFL', 2222, '', '0000-00-00', NULL, '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, 'accept'),
(11, '3323-NFI', 'NFI', 3323, '', '0000-00-00', NULL, '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, 'accept'),
(12, '3322-NFL', 'NFL', 3322, '', '0000-00-00', NULL, '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, 'accept'),
(13, '5454-NFL', 'NFL', 5454, '', '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', NULL, 0, NULL, NULL, 'accept'),
(14, '5454-NFL', 'NFL', 5454, '', '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', NULL, 0, NULL, NULL, 'accept'),
(15, '1111-NFI', 'NFI', 1111, '', '0000-00-00', NULL, '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, 'accept'),
(16, '1111-NFI', 'NFI', 1111, '', '0000-00-00', NULL, '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, 'accept'),
(17, '1111-NFI', 'NFI', 1111, '', '0000-00-00', NULL, '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, 'accept'),
(18, '1111-NFI', 'NFI', 1111, '', '0000-00-00', NULL, '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, 'accept'),
(19, '5555-NFL', 'NFL', 5555, '', '0000-00-00', NULL, '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, 'accept');

-- --------------------------------------------------------

--
-- Table structure for table `homebrand`
--

CREATE TABLE `homebrand` (
  `kode_homebrand` char(3) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homebrand`
--

INSERT INTO `homebrand` (`kode_homebrand`, `nama`) VALUES
('FOI', 'Indomaret - Food'),
('FOL', 'Larrist - Food'),
('FON', 'NonHomeBrand - Food'),
('NFI', 'Indomaret - Non Food'),
('NFL', 'Larrist - Non Food'),
('NFN', 'NonHomeBrand  - Non Food');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `Username` varchar(20) NOT NULL,
  `Password` char(8) NOT NULL,
  `Nama` varchar(50) DEFAULT NULL,
  `Email` varchar(30) DEFAULT NULL,
  `No_Telp` int(15) DEFAULT NULL,
  `Status_user` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dtl_design`
--
ALTER TABLE `dtl_design`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_kdhomebrand` (`kode_homebrand`);

--
-- Indexes for table `homebrand`
--
ALTER TABLE `homebrand`
  ADD PRIMARY KEY (`kode_homebrand`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`Username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dtl_design`
--
ALTER TABLE `dtl_design`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dtl_design`
--
ALTER TABLE `dtl_design`
  ADD CONSTRAINT `dtl_design_ibfk_1` FOREIGN KEY (`kode_homebrand`) REFERENCES `homebrand` (`kode_homebrand`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
